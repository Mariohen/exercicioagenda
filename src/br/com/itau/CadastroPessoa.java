package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CadastroPessoa {

    int quantidadePessoas;

    public CadastroPessoa(int quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
  }

    public List<Pessoa> lista(){
        ArrayList<Pessoa> lista = new ArrayList<>();
       for (int i = 0; i < this.quantidadePessoas; i++) {
            Pessoa pessoa = new Pessoa();
            Scanner scanner = new Scanner(System.in);
            System.out.print("Informe o nome: ");
            pessoa.setNome(scanner.nextLine());
            System.out.print("Informe o telefone: ");
            pessoa.setTelefone(scanner.nextLine());
            System.out.print("Informe o email: ");
            pessoa.setEmail(scanner.nextLine());

           lista.add(pessoa);
       }

       return lista;
    }

}


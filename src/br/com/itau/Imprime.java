package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Imprime {

    public void imprimeLista (List<Pessoa> imprimeLista){

       for (int i = 0; i < imprimeLista.size(); i++) {
           System.out.println("Nome      : " + imprimeLista.get(i).getNome());
           System.out.println("Telefone  : " + imprimeLista.get(i).getTelefone());
           System.out.println("Email     : " + imprimeLista.get(i).getEmail());
        };
    }
}

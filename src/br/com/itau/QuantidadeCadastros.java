package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class QuantidadeCadastros {

    int quantidade;
    public QuantidadeCadastros() {
        cadastroInicial();
    }

    public int cadastroInicial(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Informe quantas pessoas deverão ser cadastradas: ");
        quantidade = scanner.nextInt();
        return quantidade;
    }
}
